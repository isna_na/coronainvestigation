import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
 

import {Actions} from 'react-native-router-flux';
import { color } from 'react-native-reanimated';
 
export default class Profile extends Component {
 

    render() {
        return(
            <View style={styles.container}>
                <Text>{'\n'}</Text>
                <Text>{'\n'}</Text>
                <Text style={{fontSize:32, marginBottom:20}}>About Us</Text>
                <View style={styles.container2}>
                    <Text>Developer</Text>
                    <Text>1. Name : Vania Zerlinda</Text>
                    <Text>2.  Name : Isna Nur Aini</Text>
                    <Text> Detail 1 :</Text>
                    <Text> email : vaniazerlinda19@gmail.com</Text>
                    <Text>gitlab : vaniazer</Text>
                    <Text>telegram : vaniazer</Text>
                    <Text>Phone : 0895392847031</Text>
                    <Text> Detail 2 :</Text>
                    <Text> email : isnamaku8@gmail.com</Text>
                    <Text>gitlab : isna_na</Text>
                    <Text>telegram : isnanaa</Text>
                    <Text>Phone : 085600934634</Text>

                </View>
                
            </View>
        )
    }
}
 
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white'
    },
    container2 :{
      
        width: "80%",
        height:"60%",
        
        backgroundColor: 'yellow',
        borderRadius:17
    },
    signupTextCont: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
      paddingVertical: 16,
      flexDirection: 'row'
    },
    signupText: {
      color: '#12799f', 
      fontSize:16
    },
    signupButton: {
        color: '#12799f',
        fontSize:16,
        fontWeight: '500'
    }
});