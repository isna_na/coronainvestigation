import React from 'react';
import Axios from "axios";
import "./style.css";
import { StyleSheet, Text, View, TextInput, TouchableOpacity, AsyncStorage, Keyboard, Button } from 'react-native';

import {Actions} from 'react-native-router-flux';
 
import Form from '../components/Form';

export default class Home extends React.Component {
    constructor(props) {
        super(props);

        this.getCountryData = this.getCountryData.bind(this);
    }
    state = {
        confirmed: 0,
        recovered: 0,
        deaths: 0,
        countries: []
    }
    componentDidMount() {
        this.getData();
    }
    async getData(){
        const resApi = await Axios.get("https://covid19.mathdro.id/api");
        const resCountries = await Axios.get("https://covid19.mathdro.id/api/countries");
        const countries = [];
        for(var i = 0; i < resCountries.data.countries.length; i++){
            countries.push(resCountries.data.countries[i].name);
        }
        this.setState({
            confirmed: resApi.data.confirmed.value,
            recovered: resApi.data.recovered.value,
            deaths: resApi.data.deaths.value,
            countries
        });
    }

    async getCountryData(e){
        if (e.target.value === "Worldwide"){
            return this.getData();
        }
        try {
            const res = await Axios.get(`https://covid19.mathdro.id/api/countries/${e.target.value}`);
            this.setState({
                confirmed: res.data.confirmed.value,
                recovered: res.data.recovered.value,
                deaths: res.data.deaths.value
            })}
        catch (err) {
            if(err.response.status === 404)
                this.setState({
                    confirmed: "No data available",
                    recovered: "No data available",
                    deaths: "No data available"
                })
        }
    }
    renderCountryOptions(){
        return this.state.countries.map((country,i) => {
            return <option key={i}>{country}</option>
        });
    }
 
    profile(){
        Actions.profile();
    }
 
    render() {
        return(
                <View className="container">
                    <h1>Corona Update</h1>

                    <select className="dropDown" onChange={this.getCountryData}>
                        <option>Worldwide</option>
                        {this.renderCountryOptions()}
                    </select>

                    <div className="flex">
                        <div className="box confirmed">
                            <h3>Confirmed</h3>
                            <h4>{this.state.confirmed}</h4>
                        </div>
                        <div className="box recovered">
                            <h3>Recovered</h3>
                            <h4>{this.state.recovered}</h4>
                        </div>
                        <div className="box deaths">
                            <h3>Deaths</h3>
                            <h4>{this.state.deaths}</h4>
                        </div>
                    </div>

                    <Text style={{marginBottom:50}}>Ini Home</Text>
                    <Text style={styles.signupText}>Find Out Us? </Text>
                    <TouchableOpacity onPress={this.profile}><Text style={styles.signupButton}>Click Here</Text></TouchableOpacity>
                </View>
        );
    }
}
 
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
    },
    signupTextCont: {
      flexGrow: 1,
      justifyContent: 'center',
      alignItems: 'flex-end',
      paddingVertical: 16,
      flexDirection: 'row',
    },
    signupText: {
      color: '#12799f', 
      fontSize:16,
    },
    signupButton: {
        color: '#12799f',
        fontSize:16,
        fontWeight: '500',
    }
});