
import React, { Component } from 'react';
import {Router, Stack, Scene} from 'react-native-router-flux';
import {HeaderButton } from 'react-navigation-header-buttons';
import { MaterialIcons } from '@expo/vector-icons'; 

 
import Login from './pages/Login';
import Signup from './pages/Signup';
import Form from './components/Form';
import Home from './pages/Home';
import { Button } from 'react-native';
import Profile from './pages/Profile';


 
 
export default class Routes extends Component {
    render() {
        return (
            
            <Router barButtonIconStyle ={styles.barButtonIconStyle}
                hideNavBar={false} 
                navigationBarStyle={{backgroundColor: '#1565c0',}} 
                titleStyle={{color: 'white',}}
            >
               
                <Stack key="root" backgroundColor='white' color='white'>
                <Scene key="login" component={Login} title="Login"/>
                <Scene key="signup" component={Signup} title="Sign up"/>
                <Stack key="profile" component={Profile} title="Profile" />

                <Stack
                key="home"
            component={Home}
               title={"Home"}
              
               
      />       
                <Scene key="form" component={Form} title="Form"/>
                </Stack>
            </Router>
        )
    }
}
 
const styles = {
    barButtonIconStyle: {
        tintColor: 'white'
    }
}